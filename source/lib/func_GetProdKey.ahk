﻿; Drugwash, 2013-2022
; v1.1
;================================================================
GetPK(DPID, t=0)					; Calculate Product key from DigitalProductId
;================================================================
{
VarSetCapacity(buf, StrLen(DPID)/2, 0)									; Create binary buffer
Loop, % StrLen(DPID)/2
	NumPut("0x" SubStr(DPID, 2*A_Index-1, 2), buf, A_Index-1, "UChar")	; Move string to buffer
idx := !t ? 51 : t=1 ? 807 : 0											; DigitalProductId / DigitalProductId4
if !idx
	return "Error: bad installation type (" t ") in " A_ThisFunc "()"
chk := NumGet(buf, idx+15, "UChar")
if (chk / 6) & 1
	NumPut(chk & 0xF7, buf, idx+15, "UChar")	; correction for Win8+
VarSetCapacity(code, 15)											; Create working buffer
Loop, 15
	NumPut(NumGet(buf, A_Index+idx, "UChar"), code, A_Index-1, "UChar")	; Fill working buffer
kstr := "BCDFGHJKMPQRTVWXY2346789"
res=
Loop, 25
	{
	a=0
	Loop, 15
		{
		a *= 256
		a += NumGet(code, 15-A_Index, "UChar")
		NumPut((a//24) & 255, code, 15-A_Index, "UChar")
		a := Mod(a, 24)
		end := a
		}
	res := SubStr(kstr, a+1, 1) res
	}
if is8
	{
	StringMid, res1, res, 2, end
	StringTrimLeft, res2, res, end+1
	res := res1 "N" res2
	}
VarSetCapacity(buf, 0), VarSetCapacity(code, 0)
return RegExReplace(res, "(.{5})(.{5})(.{5})(.{5})(.{5})", "$1-$2-$3-$4-$5")
}
