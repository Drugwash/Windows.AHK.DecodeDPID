; � Drugwash May 2014-August 2015
; requires updates.ahk

GetOSver(nice="")
{
Global Ptr, AW
Static suite="Small Business Server,Enterprise,BackOffice,,Terminal Services,Small Business Server (restricted),Embedded,DataCenter,Remote Desktop (single user),Home,Web Edition,,,Storage Server,Compute Cluster Edition"
off := 128*2**(A_IsUnicode=TRUE)
sSize := 28+off
VarSetCapacity(OVIX, sSize, 0)	; OSVERSIONINFOEX struct
NumPut(sSize, OVIX, 0, "UInt")
if !res := DllCall("GetVersionEx" AW, Ptr, &OVIX, "UInt")
	{
	sSize := 20+off
	VarSetCapacity(OVIX, sSize, 0)	; OSVERSIONINFO struct (for Win95)
	NumPut(sSize, OVIX, 0, "UInt")
	if !res := DllCall("GetVersionEx" AW, Ptr, &OVIX, "UInt")
		return
	}
d2 := NumGet(OVIX, 4, "UInt")
d3 := NumGet(OVIX, 8, "UInt")
d4 := NumGet(OVIX, 12, "UInt") & 0xFFFF
if !nice
	return d2 "." d3 "." d4
VarSetCapacity(d6, off, 0)
DllCall("lstrcpyn" AW, UInt, &d6, UInt, &OVIX + 20, UInt, 128)
VarSetCapacity(d6, -1)
d6=%d6%
if A_OSVersion in WIN_95,WIN_98,WIN_ME
	return d2 "." d3 "." d4 " " d6
d9a := NumGet(OVIX, 24+off, "UShort")
d10a := NumGet(OVIX, 26+off, "UChar")
d9=
Loop, Parse, suite, CSV
	if (d9a & 2**(A_Index-1))
		d9 .= A_LoopField " + "
StringTrimRight, d9, d9, 3
d10 := d10a=1 ? "Workstation" : d10a=2 ? "Domain controller" : d10a=3 ? "Server" : ""
SysGet, d11a, 86		; Tablet PC
SysGet, d11b, 87		; Media Center
SysGet, d11c, 88		; Starter Edition
SysGet, d11d, 89		; Server 2003 R2
d11 := d11a ? " Tablet PC " : d11b ? " Media Center " : d11c ? " Starter Edition " : d11d ? " Server 2003 R2 " : " "
r := d2 "." d3 "." d4 " " d6 d11 d9 " " d10
r=%r%
return r
}
